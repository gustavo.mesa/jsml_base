/*
    Gustavo Adolfo Mesa Roldán.
    Proyecto jsml 0.4 (Motor de renderizado)
*/

$( document ).ready(function() {
    var tagsVoid = ["area", "base", "br", "col", "command", "embed", "hr", "img", "input", "keygen", "link", "meta", "param", "source", "track", "wbr"];
    var currentLoc = window.location.pathname;
    var spawnVel = 400;

    var setTagName = x => x ? x : 'div';
    var setAttribute = (x,y) => x ? y+'="'+x+'"' : '';
    var setAttributes = x => x ? $.map(x, (a,k) => k+'="'+a+'"').join(' ') : '';
    var setContent = x => Array.isArray(x) ? setInterContent(x) : x ? x : '';
    var createTag = x => '<'+setTagName(x.type)+' '+setAttribute(x.id, 'id')+' '+setAttribute(x.class, 'class')+' '+setAttributes(x.attributes)+'>'+(tagsVoid.includes(x.type) ? '' : setContent(x.content)+'</'+setTagName(x.type)+'>');
    var setInterContent = x => x.map(item => (typeof item === 'string' || item instanceof String) ? item : createTag(item)).join(' ');
    var setTag = x => (typeof x === 'string' || x instanceof String) ? x : $(createTag(x));
    var getUrl = _ => currentLoc == '/index.html' || currentLoc == '/' ? '/index' : currentLoc;
    var setStructure = x => $.each(JSON.parse(x), ( key, value ) => value.map(item => $(key).append(setTag(item))));
    var getData = (t,x) => {$(t).fadeOut(0);$.get(x, data => { setStructure(data) }).done(_ => $(window).load(_ => $(t).fadeIn(spawnVel))).fail(_  => getData('body', '/jsml/404.jsml'))};

    var init = _ => getData('body', '/jsml'+getUrl()+'.jsml');
    init();
});